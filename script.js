
document.addEventListener('DOMContentLoaded', function () {
    const keys = document.querySelectorAll('.key');
  
    keys.forEach(function (key) {
      key.addEventListener('click', function () {
        highlightKey(key);
      });
    });
  
    document.addEventListener('keydown', function (event) {
      const pressedKey = event.key.toUpperCase();
      const targetKey = document.querySelector(`.key[data-key="${pressedKey}"]`);
  
      if (targetKey) {
        highlightKey(targetKey);
      }
    });
  
    function highlightKey(targetKey) {
      keys.forEach(function (key) {
        key.classList.remove('active');
      });
  
      targetKey.classList.add('active');
    }
  });
        
    















